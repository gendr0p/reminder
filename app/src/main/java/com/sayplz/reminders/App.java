package com.sayplz.reminders;

import android.app.Application;

import com.sayplz.reminders.data.DatabaseHelper;

/**
 * Created by sayplz on 24.08.16.
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        DatabaseHelper.getInstance(this);
    }
}
