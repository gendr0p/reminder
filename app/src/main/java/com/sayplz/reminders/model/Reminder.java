package com.sayplz.reminders.model;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by sayplz on 24.08.16.
 */
public class Reminder implements Serializable {
    public static final String TABLE_NAME = "reminder";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_NOTES = "notes";
    public static final String COLUMN_START = "start";
    public static final String COLUMN_FREQUENCY = "frequency";
    public static final String COLUMN_DURATION = "duration";

    private long id;
    private String description;
    private String notes;
    private Date start;
    private int frequency;
    private int duration;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getRemaining() {
        Date finish = new Date(this.start.getTime() + TimeUnit.DAYS.toMillis(this.duration));
        int remaining = (int) (finish.getTime() - new Date().getTime()) / 24 / 60 / 60 / 1000 + 1;
        if (remaining == 1) {
            return "1 day";
        }
        if (remaining > 0) {
            return String.format("%d days", remaining);
        }
        return "0";
    }

    public Date getFinishDate() {
        Date finish = new Date(this.start.getTime() + TimeUnit.DAYS.toMillis(this.duration));
        return finish;
    }
}
