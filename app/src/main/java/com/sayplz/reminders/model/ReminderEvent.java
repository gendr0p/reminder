package com.sayplz.reminders.model;

import java.util.Date;

/**
 * Created by sayplz on 24.08.16.
 */
public class ReminderEvent {
    public static final String TABLE_NAME = "reminder_event";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_REMINDER_ID = "reminder_id";
    public static final String COLUMN_EVENT = "event";

    private long id;
    private long reminderId;
    private Date event;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getEvent() {
        return event;
    }

    public void setEvent(Date event) {
        this.event = event;
    }

    public long getReminderId() {
        return reminderId;
    }

    public void setReminderId(long reminderId) {
        this.reminderId = reminderId;
    }
}
