package com.sayplz.reminders.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.sayplz.reminders.R;
import com.sayplz.reminders.data.ReminderEventDao;
import com.sayplz.reminders.model.Reminder;
import com.sayplz.reminders.model.ReminderEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sayplz on 24.08.16.
 */

public class DetailsActivity extends AppCompatActivity {
    private TextView description;
    private TextView notes;
    private ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Reminder reminder = (Reminder) getIntent().getSerializableExtra("reminder");

        description = (TextView) findViewById(R.id.description_details);
        notes = (TextView) findViewById(R.id.notes_detail);
        list = (ListView) findViewById(R.id.list_details);

        description.setText(reminder.getDescription());
        notes.setText(String.format("%s: %s", getString(R.string.notes), reminder.getNotes()));


        List<String> events = new ArrayList<>();
        DateFormat df = new DateFormat();
        for (ReminderEvent event : ReminderEventDao.getInstance(this).getAll(reminder.getId())) {
            events.add(String.valueOf(df.format("MM/dd/yyyy hh:mm", event.getEvent())));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.event_row, events);
        list.setAdapter(adapter);
    }
}
