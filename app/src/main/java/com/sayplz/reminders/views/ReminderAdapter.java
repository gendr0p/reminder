package com.sayplz.reminders.views;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sayplz.reminders.R;
import com.sayplz.reminders.model.Reminder;
import com.sayplz.reminders.util.Strings;

import java.util.List;

/**
 * Created by sayplz on 24.08.16.
 */
public class ReminderAdapter extends RecyclerView.Adapter<ReminderAdapter.ViewHolder> {
    private final IDeleteListener listener;
    private List<Reminder> reminders;
    private Context context;

    public ReminderAdapter(List<Reminder> reminders, Context context, IDeleteListener listener) {
        this.reminders = reminders;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ReminderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_reminder, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ReminderAdapter.ViewHolder holder, int position) {
        final Reminder reminder = reminders.get(position);

        holder.description.setText(reminder.getDescription());
        holder.description.setPaintFlags(holder.description.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onDescriptionClick(reminder);
            }
        });

        holder.frequency.setText(Strings.minutesToString(reminder.getFrequency()));
        holder.remaining.setText(reminder.getRemaining());

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(R.string.sure)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                listener.onDelete(reminder);
                            }
                        })
                        .setNegativeButton(R.string.no, null)
                        .show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return reminders.size();
    }

    public void setReminders(List<Reminder> reminders) {
        this.reminders = reminders;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView description;
        private TextView frequency;
        private TextView remaining;
        private ImageView delete;

        public ViewHolder(View itemView) {
            super(itemView);

            description = (TextView) itemView.findViewById(R.id.description_add);
            frequency = (TextView) itemView.findViewById(R.id.frequency_add);
            remaining = (TextView) itemView.findViewById(R.id.remaining);
            delete = (ImageView) itemView.findViewById(R.id.delete);
        }
    }

    public interface IDeleteListener {
        void onDelete(Reminder reminder);

        void onDescriptionClick(Reminder reminder);
    }
}
