package com.sayplz.reminders.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.sayplz.reminders.R;
import com.sayplz.reminders.data.ReminderDao;
import com.sayplz.reminders.model.Reminder;
import com.sayplz.reminders.service.AlarmService;

import java.util.List;

public class MainActivity extends AppCompatActivity implements ReminderAdapter.IDeleteListener {

    private RecyclerView list;
    private ReminderAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        list = (RecyclerView) findViewById(R.id.list);
        list.setLayoutManager(new LinearLayoutManager(this));

        List<Reminder> reminders = ReminderDao.getInstance(this).getAll();
        adapter = new ReminderAdapter(reminders, this, this);
        list.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });

        startService(new Intent(MainActivity.this, AlarmService.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateTimers();
    }

    private void updateTimers() {
        List<Reminder> reminders = ReminderDao.getInstance(this).getAll();
        adapter.setReminders(reminders);

        Intent intent = new Intent(MainActivity.this, AlarmService.class);
        stopService(intent);
        startService(intent);
    }

    @Override
    public void onDelete(Reminder reminder) {
        ReminderDao.getInstance(this).remove(reminder.getId());
        updateTimers();
    }

    @Override
    public void onDescriptionClick(Reminder reminder) {
        Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
        intent.putExtra("reminder", reminder);
        MainActivity.this.startActivity(intent);
    }

}
