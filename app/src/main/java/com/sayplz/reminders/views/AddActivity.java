package com.sayplz.reminders.views;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.sayplz.reminders.R;
import com.sayplz.reminders.data.ReminderDao;
import com.sayplz.reminders.model.Reminder;
import com.sayplz.reminders.util.Strings;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by sayplz on 24.08.16.
 */
public class AddActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText description;
    private EditText notes;
    private EditText start;
    private Spinner frequency;
    private Spinner duration;
    private Date startDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        description = (EditText) findViewById(R.id.description_add);
        notes = (EditText) findViewById(R.id.notes_add);

        start = (EditText) findViewById(R.id.start_add);
        DateFormat df = new DateFormat();
        startDate = new Date();
        start.setText(df.format("MM/dd/yyyy hh:mm", startDate));

        frequency = (Spinner) findViewById(R.id.frequency_add);
        duration = (Spinner) findViewById(R.id.duration_add);

        Button save = (Button) findViewById(R.id.save_add);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    save();
                    finish();
                }
            }
        });

        ImageButton picker = (ImageButton) findViewById(R.id.datetime_picker);
        picker.setOnClickListener(this);


    }

    private void save() {
        Reminder reminder = new Reminder();
        reminder.setDescription(description.getText().toString());
        reminder.setNotes(notes.getText().toString());
        reminder.setStart(startDate);

        String _temp = frequency.getSelectedItem().toString();
        reminder.setFrequency(Strings.stringToMinutes(_temp));

        _temp = duration.getSelectedItem().toString();
        int dur = Integer.valueOf(_temp.substring(0, _temp.indexOf(" day")));
        reminder.setDuration(dur);

        ReminderDao.getInstance(this).add(reminder);
    }

    private boolean validate() {
        boolean check = true;

        if (start.length() == 0) {
            check = false;
            start.setError(getString(R.string.error_fill_datetime));
        }

        if (description.length() == 0) {
            check = false;
            description.setError(getString(R.string.error_fill_description));
        }

        if (notes.length() == 0) {
            check = false;
            notes.setError(getString(R.string.error_fill_notes));
        }

        return check;
    }

    @Override
    public void onClick(View view) {
        final View dialogView = View.inflate(AddActivity.this, R.layout.date_time_picker, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(AddActivity.this).create();

        dialogView.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePicker datePicker = (DatePicker) dialogView.findViewById(R.id.date_picker);
                TimePicker timePicker = (TimePicker) dialogView.findViewById(R.id.time_picker);

                Calendar calendar = new GregorianCalendar(datePicker.getYear(),
                        datePicker.getMonth(),
                        datePicker.getDayOfMonth(),
                        timePicker.getCurrentHour(),
                        timePicker.getCurrentMinute());

                DateFormat df = new DateFormat();
                startDate = calendar.getTime();
                start.setText(df.format("MM/dd/yyyy hh:mm", calendar.getTime()));
                alertDialog.dismiss();
            }
        });
        alertDialog.setView(dialogView);
        alertDialog.show();
    }
}
