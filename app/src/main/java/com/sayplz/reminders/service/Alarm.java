package com.sayplz.reminders.service;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;

import com.sayplz.reminders.R;
import com.sayplz.reminders.data.ReminderDao;
import com.sayplz.reminders.data.ReminderEventDao;
import com.sayplz.reminders.model.Reminder;
import com.sayplz.reminders.model.ReminderEvent;
import com.sayplz.reminders.views.DetailsActivity;
import com.sayplz.reminders.views.CancelActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sayplz on 25.08.16.
 */

public class Alarm extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        PowerManager pm = (PowerManager) context.getApplicationContext().getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "service");
        wakeLock.acquire();


        List<Reminder> reminders = (List<Reminder>) intent.getSerializableExtra("reminder");

        if (reminders != null) {
            for (Reminder reminder : reminders) {
                ReminderEvent reminderEvent = new ReminderEvent();
                reminderEvent.setEvent(new Date());
                reminderEvent.setReminderId(reminder.getId());
                ReminderEventDao.getInstance(context).add(reminderEvent);

                createNotifation(context, reminder);
            }
        }

        setAlarm(context);

        wakeLock.release();
    }

    public void setAlarm(Context context) {
        cancelAlarm(context);

        Long minNext = null;
        List<Reminder> reminders = new ArrayList<>();
        Date current = new Date();
        for (Reminder reminder : ReminderDao.getInstance(context).getAll()) {
            Date start = reminder.getStart();
            Date finish = reminder.getFinishDate();

            if (current.after(finish)) {
                continue;
            }

            int freq = reminder.getFrequency() * 60 * 1000; // get milliseconds
            int cnt = (int) ((current.getTime() - start.getTime()) / freq) + 1;
            Long nextMillisec = (start.getTime() + freq * cnt) - current.getTime(); // next event
            if (current.before(start)) {
                nextMillisec = start.getTime() - current.getTime();
            }
            if (nextMillisec > 0 && (minNext == null || minNext >= nextMillisec)) {
                minNext = nextMillisec;
                reminders.add(reminder);
            }
        }

        if (minNext != null) {
            Log.d("DEBUG", String.format("next alarm: %s", minNext));
            AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent i = new Intent(context, Alarm.class);
            i.putExtra("reminder", (Serializable) reminders);
            PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
            am.set(AlarmManager.RTC_WAKEUP, current.getTime() + minNext, pi);
        }
    }

    public void cancelAlarm(Context context) {
        Intent intent = new Intent(context, Alarm.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void createNotifation(Context context, Reminder reminder) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra("reminder", reminder);
        PendingIntent pIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), intent, 0);

        Intent cancelIntent = new Intent(context, CancelActivity.class);
        cancelIntent.putExtra("id", reminder.getId());
        PendingIntent pIntentCancel = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), cancelIntent, 0);

        Notification notification = new Notification.Builder(context)
                .setContentTitle(reminder.getDescription())
                .setContentText(reminder.getNotes())
                .setSmallIcon(R.drawable.ic_calendar)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .addAction(R.drawable.ic_ok, context.getString(R.string.button_ok), pIntentCancel)
                .addAction(R.drawable.ic_view, context.getString(R.string.button_view), pIntent)
                .build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(0, notification);
    }
}
