package com.sayplz.reminders.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sayplz.reminders.model.ReminderEvent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sayplz on 24.08.16.
 */
public class ReminderEventDao {
    private static ReminderEventDao instance;

    private DatabaseHelper dh;

    public synchronized static ReminderEventDao getInstance(Context context) {
        if (instance == null) instance = new ReminderEventDao(context);
        return instance;
    }

    private ReminderEventDao(Context context) {
        dh = DatabaseHelper.getInstance(context);
    }

    private ReminderEvent fetch(Cursor cursor) {
        ReminderEvent reminderEvent = new ReminderEvent();
        reminderEvent.setId(cursor.getLong(cursor.getColumnIndex(ReminderEvent.COLUMN_ID)));
        reminderEvent.setReminderId(cursor.getLong(cursor.getColumnIndex(ReminderEvent.COLUMN_REMINDER_ID)));
        reminderEvent.setEvent(new Date(cursor.getLong(cursor.getColumnIndex(ReminderEvent.COLUMN_EVENT))));

        return reminderEvent;
    }

    public void add(ReminderEvent reminderEvent) {
        SQLiteDatabase db = dh.getDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(ReminderEvent.COLUMN_REMINDER_ID, reminderEvent.getReminderId());
            values.put(ReminderEvent.COLUMN_EVENT, reminderEvent.getEvent().getTime());

            db.insert(ReminderEvent.TABLE_NAME, null, values);
        } finally {
            db.close();
        }
    }

    public List<ReminderEvent> getAll(long id) {
        SQLiteDatabase db = dh.getDatabase();
        try {
            List<ReminderEvent> reminderEvents = new ArrayList<>();

            Cursor cursor = db.rawQuery(
                    String.format("SELECT * FROM %s WHERE %s = '%s'", ReminderEvent.TABLE_NAME, ReminderEvent.COLUMN_REMINDER_ID, id), null);

            if (cursor.moveToFirst()) {
                do {
                    ReminderEvent reminderEvent = fetch(cursor);
                    reminderEvents.add(reminderEvent);
                } while (cursor.moveToNext());
            }
            cursor.close();

            return reminderEvents;
        } finally {
            db.close();
        }
    }
}
