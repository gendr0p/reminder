package com.sayplz.reminders.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sayplz.reminders.model.Reminder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sayplz on 24.08.16.
 */

public class ReminderDao {
    private static ReminderDao instance;

    private DatabaseHelper dh;

    public synchronized static ReminderDao getInstance(Context context) {
        if (instance == null) instance = new ReminderDao(context);
        return instance;
    }

    private ReminderDao(Context context) {
        dh = DatabaseHelper.getInstance(context);
    }

    private Reminder fetch(Cursor cursor) {
        Reminder reminder = new Reminder();
        reminder.setId(cursor.getLong(cursor.getColumnIndex(Reminder.COLUMN_ID)));
        reminder.setDescription(cursor.getString(cursor.getColumnIndex(Reminder.COLUMN_DESCRIPTION)));
        reminder.setNotes(cursor.getString(cursor.getColumnIndex(Reminder.COLUMN_NOTES)));
        reminder.setStart(new Date(cursor.getLong(cursor.getColumnIndex(Reminder.COLUMN_START))));
        reminder.setFrequency(cursor.getInt(cursor.getColumnIndex(Reminder.COLUMN_FREQUENCY)));
        reminder.setDuration(cursor.getInt(cursor.getColumnIndex(Reminder.COLUMN_DURATION)));

        return reminder;
    }

    public void add(Reminder reminder) {
        SQLiteDatabase db = dh.getDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(Reminder.COLUMN_DESCRIPTION, reminder.getDescription());
            values.put(Reminder.COLUMN_NOTES, reminder.getNotes());
            values.put(Reminder.COLUMN_START, reminder.getStart().getTime());
            values.put(Reminder.COLUMN_FREQUENCY, reminder.getFrequency());
            values.put(Reminder.COLUMN_DURATION, reminder.getDuration());

            db.insert(Reminder.TABLE_NAME, null, values);
        } finally {
            db.close();
        }
    }

    public List<Reminder> getAll() {
        SQLiteDatabase db = dh.getDatabase();
        try {
            List<Reminder> reminders = new ArrayList<>();

            Cursor cursor = db.rawQuery(
                    String.format("SELECT * FROM %s", Reminder.TABLE_NAME), null);

            if (cursor.moveToFirst()) {
                do {
                    Reminder reminder = fetch(cursor);
                    reminders.add(reminder);
                } while (cursor.moveToNext());
            }
            cursor.close();

            return reminders;
        } finally {
            db.close();
        }
    }

    public void remove(long id) {
        SQLiteDatabase db = dh.getDatabase();
        try {
            db.execSQL(String.format("DELETE FROM %s WHERE _id = %s", Reminder.TABLE_NAME, id));
        } finally {
            db.close();
        }
    }
}