package com.sayplz.reminders.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.sayplz.reminders.model.Reminder;
import com.sayplz.reminders.model.ReminderEvent;

/**
 * Created by sayplz on 24.08.16.
 */

public final class DatabaseHelper extends SQLiteOpenHelper {
    // Query for create table
    private static final String CREATE_TABLE_REMINDER = String.format(
            "CREATE TABLE %s (" +
                    "%s INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    "%s TEXT," +
                    "%s TEXT," +
                    "%s INTEGER," +
                    "%s INTEGER," +
                    "%s INTEGER" +
                    ")", Reminder.TABLE_NAME, Reminder.COLUMN_ID, Reminder.COLUMN_DESCRIPTION,
            Reminder.COLUMN_NOTES, Reminder.COLUMN_START,
            Reminder.COLUMN_FREQUENCY, Reminder.COLUMN_DURATION);

    private static final String CREATE_TABLE_REMINDEREVENT = String.format(
            "CREATE TABLE %s (" +
                    "%s INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    "%s INTEGER," +
                    "%s INTEGER" +
                    ")", ReminderEvent.TABLE_NAME, ReminderEvent.COLUMN_ID, ReminderEvent.COLUMN_REMINDER_ID, ReminderEvent.COLUMN_EVENT);

    private static final String DATABASE_NAME = "reminders.db";
    private static int DATABASE_VERSION = 1;

    private static DatabaseHelper instance;

    public synchronized static DatabaseHelper getInstance(Context context) {
        if (instance == null) instance = new DatabaseHelper(context);
        return instance;
    }

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_REMINDER);
        db.execSQL(CREATE_TABLE_REMINDEREVENT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public SQLiteDatabase getDatabase() {
        return getWritableDatabase();
    }
}