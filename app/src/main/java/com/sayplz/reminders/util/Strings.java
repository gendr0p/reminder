package com.sayplz.reminders.util;

import java.util.Locale;

/**
 * Created by sayplz on 24.08.16.
 */

public class Strings {

    public static int stringToMinutes(String s) {
        try {
            int hours = Integer.valueOf(s.substring(0, 2));
            int minutes = Integer.valueOf(s.substring(3, 5));

            return hours * 60 + minutes;
        } catch (Exception e) {
            return 0;
        }
    }

    public static String minutesToString(int frequency) {
        String time = "";

        int hours = frequency / 60;
        if (hours > 0) {
            time = String.format("%dh", frequency / 60);
        }

        int min = frequency % 60;
        if (min > 0) {
            time += String.format(" %dm", frequency % 60);
        }
        return time;
    }
}
